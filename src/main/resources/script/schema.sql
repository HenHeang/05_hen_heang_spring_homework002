create table public.customer
(
    id      serial
        primary key,
    name    varchar(30) not null,
    address varchar(200),
    phone   varchar
);

alter table public.customer
    owner to postgres;

create table invoice(
    id serial4 PRIMARY KEY,
    invoice_date date NOT NULL ,
    customer_id  integer references customer(id) On delete CASCADE  on update CASCADE

)  ;

create  table product(
    product_id serial4 primary key ,
    product_name varchar not null ,
    product_price VARCHAR(50)
);

create table invoice_detail(
    id serial4 primary key,
    invoice_id Integer  references invoice(id) On delete CASCADE  on update CASCADE,
    product_id INTEGER references invoice(id)  On delete CASCADE On UPDATE CASCADE

) ;


