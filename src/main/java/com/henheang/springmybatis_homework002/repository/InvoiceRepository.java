package com.henheang.springmybatis_homework002.repository;

import com.henheang.springmybatis_homework002.model.entity.Invoice;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

    
    @Select("SELECT * FROM invoice ")
    
    @Results(
            id = "invoiceMapper",
            value = {
                    @Result(property = "invoiceId", column = "id"),
                    @Result(property = "invoiceDate", column = "invoice_date"),
                    @Result(property = "customer", column = "customer_id",
                            one = @One(select = "com.henheang.springmybatis_homework002.repository.CustomerRepository.getCustomerById")),
                    @Result(property = "products", column = "id",
                            many = @Many(select="com.henheang.springmybatis_homework002.repository.ProductRepository.getProductByInvoiceId")
                    )
//
            })
//
    List<Invoice> findAllInvoice();

    @Select("Select*FROM invoice where id = #{InvoiceId}")
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById(Integer invoiceId);
}
