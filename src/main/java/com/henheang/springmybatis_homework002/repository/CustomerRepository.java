package com.henheang.springmybatis_homework002.repository;

import com.henheang.springmybatis_homework002.model.entity.Customer;
import com.henheang.springmybatis_homework002.model.request.RequestCustomer;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    //select for all data
    @Select("Select * From customer")
    List<Customer> findAllCustomer();



    //for insert data
    @Select("""
            INSERT INTO customer VALUES
            (DEFAULT, #{customer.name}, #{customer.address}, #{customer.phone})
            RETURNING *;
            """)
    List<Customer> addCustomer(@Param("customer") RequestCustomer requestCustomer);


    //Delete data
    @Delete("DELETE FROM customer where id = #{id}")
    List<Customer> deleteCustomer(Integer id);

    //   update
    @Select("Update customer " + "Set name = #{request.name}, " +
            "address=#{request.address}, " +
            "phone=#{request.phone} " +
            "where id = #{customerId} " +
            "Returning id")
   Customer updateCustomer(@Param("request") RequestCustomer requestCustomer , Integer customerId );

    @Select("select * from customer where id = #{id}")
    @Result (property = "id",column = "id" )
    @Result (property = "name",column = "name" )
    @Result (property = "phone",column = "phone" )
    @Result (property = "address",column = "address")
    Customer getCustomerById(Integer id);
}

