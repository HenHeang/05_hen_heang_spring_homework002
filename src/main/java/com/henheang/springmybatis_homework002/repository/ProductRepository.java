package com.henheang.springmybatis_homework002.repository;

import com.henheang.springmybatis_homework002.model.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("""
            SELECT p.product_id, p.product_name, p.product_price
            From invoice_detail pb INNER JOIN product p ON p.product_id = pb.product_id
            WHERE pb.invoice_id = #{invoiceId}
            """)
    @Result(property = "productId", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_price")

    List<Product>getProductByInvoiceId(Integer invoiceId);
     @Select("Select *FROM product")
    List<Product> getAllProduct();
    @Select("select * from product where id = #{id}")
//    @Result (property = "id",column = "id" )
//    @Result (property = "name",column = "name" )
//    @Result (property = "phone",column = "phone" )
//    @Result (property = "address",column = "address")

    Product getProductById();
}
