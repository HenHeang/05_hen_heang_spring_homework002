package com.henheang.springmybatis_homework002.service;

import com.henheang.springmybatis_homework002.model.entity.Invoice;
import com.henheang.springmybatis_homework002.repository.InvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceImp implements InvoiceService{

    private  final InvoiceRepository invoiceRepository;

    public InvoiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public List<Invoice> getInvoiceById(Integer invoiceId) {
        return (List<Invoice>) invoiceRepository.getInvoiceById(invoiceId);
    }
}
