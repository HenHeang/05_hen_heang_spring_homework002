package com.henheang.springmybatis_homework002.service;

import com.henheang.springmybatis_homework002.model.entity.Product;
import com.henheang.springmybatis_homework002.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductImp implements ProductService{
    private final ProductRepository productRepository;

    public ProductImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {

        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById();
    }
}
