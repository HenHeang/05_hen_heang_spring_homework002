package com.henheang.springmybatis_homework002.service;

import com.henheang.springmybatis_homework002.model.entity.Invoice;

import java.util.List;

public interface InvoiceService {
      List<Invoice> getAllInvoice();
      List<Invoice>getInvoiceById(Integer invoiceId);
}
