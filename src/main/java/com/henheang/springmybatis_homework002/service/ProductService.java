package com.henheang.springmybatis_homework002.service;

import com.henheang.springmybatis_homework002.model.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;
  @Service
public interface ProductService {
    List<Product> getAllProduct();

      Product getProductById(Integer productId);
  }
