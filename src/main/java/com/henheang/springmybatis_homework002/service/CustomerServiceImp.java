package com.henheang.springmybatis_homework002.service;

import com.henheang.springmybatis_homework002.model.entity.Customer;
import com.henheang.springmybatis_homework002.model.request.RequestCustomer;
import com.henheang.springmybatis_homework002.repository.CustomerRepository;
import com.henheang.springmybatis_homework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
       private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getAuthorById(Integer authorId) {
        return null;
    }

    @Override
    public Customer getCustomerById(Integer id) {
        System.out.println(id);
        System.out.println(customerRepository.getCustomerById(id));
        return customerRepository.getCustomerById(id);
    }

    @Override
    public List<Customer> addCustomer(RequestCustomer requestCustomer) {
        return customerRepository.addCustomer(requestCustomer);
    }

    @Override
    public List<Customer> deleteCustomer(Integer customerId) {
        return customerRepository.deleteCustomer(customerId);
    }

    @Override
    public Customer updateCustomer(RequestCustomer requestCustomer, Integer customerId) {

        return customerRepository.updateCustomer(requestCustomer,customerId);
    }


}
