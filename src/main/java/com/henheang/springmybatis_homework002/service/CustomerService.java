package com.henheang.springmybatis_homework002.service;

import com.henheang.springmybatis_homework002.model.entity.Customer;
import com.henheang.springmybatis_homework002.model.request.RequestCustomer;

import java.util.List;

public interface CustomerService {
    List<Customer>getAllCustomer();

    Customer getAuthorById(Integer authorId);

    Customer getCustomerById(Integer id);

    List<Customer> addCustomer(RequestCustomer requestCustomer);

     List<Customer> deleteCustomer(Integer customerId);


    Customer updateCustomer(RequestCustomer requestCustomer, Integer customerId);
}
