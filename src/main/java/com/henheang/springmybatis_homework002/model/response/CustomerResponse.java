package com.henheang.springmybatis_homework002.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
@JsonInclude(JsonInclude.Include.NON_NULL)    
@Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
public class CustomerResponse<T> {
     private T payload;
     private String message;
     private boolean success;

}
