package com.henheang.springmybatis_homework002.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductRequest<T> {
    private T payload;
    private String message;
    private boolean success;
}
