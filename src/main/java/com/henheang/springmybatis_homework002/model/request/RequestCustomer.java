package com.henheang.springmybatis_homework002.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestCustomer {
    private String name;
    private String address;
    private String phone;
}
