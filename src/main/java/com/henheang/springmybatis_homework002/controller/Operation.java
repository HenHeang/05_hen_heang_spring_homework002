package com.henheang.springmybatis_homework002.controller;

public @interface Operation {
    String summary();
}
