package com.henheang.springmybatis_homework002.controller;

import com.henheang.springmybatis_homework002.model.entity.Customer;
import com.henheang.springmybatis_homework002.model.request.RequestCustomer;
import com.henheang.springmybatis_homework002.model.response.CustomerResponse;
import com.henheang.springmybatis_homework002.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer/")
public class CustomerRestController {
    private final CustomerService customerService;

    public CustomerRestController(CustomerService customerService) {
        this.customerService = customerService;
    }

   // Get all customer data
    @GetMapping("/get-all-customer")
    @Operation(summary = "Get all Customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer() {
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Fetch all customer is successfully!")
                .payload(customerService.getAllCustomer())
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


    //Get data by id
//    @GetMapping("/get-customer-by-id/{id}")
//    @GetMapping("/get-test/{id}")
//    public ResponseEntity<CustomerResponse<List<Customer>>> getCustomerById(@PathVariable Integer id) {
//        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
//                .message("String")
//                .payload(customerService.getCustomerById(id))
//                .success(true)
//                .build();
//        if (response.getPayload().isEmpty()) {
//            return ResponseEntity.notFound().build();
//        }
//        return ResponseEntity.ok(response);
//    }


    //    @GetMapping("/get-customer-by-id/{id}")
    //Get by id
    @GetMapping("/get-customer/{id}")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable Integer id) {

        Customer customer = customerService.getCustomerById(id) ;
        System.out.println(customer);

        CustomerResponse <Customer> response = CustomerResponse.<Customer>builder()
                .message("String")
                .payload(customer)
                .success(true)
                .build();
//        if (response.getPayload().isEmpty()) {
//            return ResponseEntity.notFound().build();
//        }
        return ResponseEntity.ok(response);
    }

    //Put data by id
    @PostMapping("/add-new-customer/{id}")
    public ResponseEntity<CustomerResponse<List<Customer>>> addCustomer(@RequestBody RequestCustomer requestCustomer) {
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("String")
                .payload(customerService.addCustomer(requestCustomer))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @PutMapping("update-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomer(
            @RequestBody RequestCustomer requestCustomer, @PathVariable("id") Integer id
    ) {
        CustomerResponse<Customer> response = new CustomerResponse<>();
        Customer customerId = customerService.updateCustomer(requestCustomer, id);
        response.setMessage("Update successfully");
        response.setSuccess(true);
        response.setPayload(customerService.getCustomerById(id));
//        (CustomerService.updateCustomer(requestCustomer,customerId)
        return ResponseEntity.ok().body(response);
    }


    //delelete
    @DeleteMapping("delete-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<List<Customer>>> deleteCustomerById(@PathVariable("id") Integer customerId) {
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("String")
                .payload(customerService.deleteCustomer(customerId))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


}
