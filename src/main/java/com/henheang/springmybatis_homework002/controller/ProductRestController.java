package com.henheang.springmybatis_homework002.controller;

import com.henheang.springmybatis_homework002.model.entity.Product;
import com.henheang.springmybatis_homework002.model.response.ProductRequest;
import com.henheang.springmybatis_homework002.service.ProductService;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductRestController {
   private final ProductService productService;

    public ProductRestController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product ")

        public ResponseEntity<ProductRequest<List<Product>>> getAllProduct() {
            ProductRequest<List<Product>> response = ProductRequest.<List<Product>>builder()
                    .message("Fetch all customer is successfully!")
                    .payload(productService.getAllProduct())
                    .success(true)
                    .build();
            return ResponseEntity.ok(response) ;
        }

        @GetMapping("get-product-by-id/{id}")
    public ResponseEntity<ProductRequest<Product>>getProductById(@PathVariable  Integer productId){
         Product product = productService.getProductById(productId);
          ProductRequest <Product> response = ProductRequest.<Product>builder()
                    .message("String")
                    .payload(product)
                    .success(true)
                    .build();

            return null;
        }

    }

