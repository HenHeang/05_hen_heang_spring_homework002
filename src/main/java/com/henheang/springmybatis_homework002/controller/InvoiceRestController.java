package com.henheang.springmybatis_homework002.controller;


import com.henheang.springmybatis_homework002.model.entity.Invoice;
import com.henheang.springmybatis_homework002.model.response.InvoiceResponse;
import com.henheang.springmybatis_homework002.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/invoice")
public class InvoiceRestController {
    private final InvoiceService invoiceService;

    public InvoiceRestController(InvoiceService invoiceService){
        this.invoiceService=invoiceService ;
    }


    //Get all data
    @GetMapping("/invoices")
    public ResponseEntity<List<Invoice>> getAllBook(){

        return ResponseEntity.ok(invoiceService.getAllInvoice());
    }

   @GetMapping("/id")
    public ResponseEntity<Invoice>getInvoiceById(@PathVariable ("id") Integer invoiceId){
       InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
               .message("Fetch all customer is successfully!")
               .payload(invoiceService.getInvoiceById(invoiceId))
               .success(true)
               .build();
        return ResponseEntity.ok((Invoice) invoiceService.getInvoiceById(invoiceId));
   }
}
