package com.henheang.springmybatis_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMyBatisHomework002Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringMyBatisHomework002Application.class, args);
    }

}
